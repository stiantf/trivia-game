export function getQuestions(category, questions, difficulty) {
    return fetch(`https://opentdb.com/api.php?amount=${questions}&category=${category}&difficulty=${difficulty}`, {

    })
    .then(response => {
        if (response.status !== 200) {
            throw new Error('Could not fetch questions')
        }
        return response.json();

    })

}